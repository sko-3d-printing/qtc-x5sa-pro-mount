QTC X5SA Pro mount
==================

This is a refined mount for the Quick Tool Change system made by ProperPrinting:
https://www.thingiverse.com/thing:3369444

The X5SA mount was initially made by FotoFieber:
https://www.thingiverse.com/thing:4337090

I took FotoFiebers design and refined/changed it a bit for my needs:
- refined the bores (they somehow didn't fit 100% on my X5SA) and changed the diameter of the pocket on the upper mounting holes so M3 washers can fit.
- added a pocket to the lower bore for the head of the bolt (ISO 7380 flat-head without flange)
- removed the cutout on the left at the back because it isn't needed on the X5SA and also changed the right cutout, so it nicely fits the belt mount.
- added a small nudge on the right front so the Titan Aero heatsink doesn't collide when mounted onto my compact tool holder ( https://gitlab.com/sko-3d-printing/qtc-titan-aero-tool-holder )


This is still a work in progress! What is there works, but I haven't tested each and every available tool holder for clearance.


###### Please read the CHANGELOG.md for a list of Updates
