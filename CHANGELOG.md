##### CHANGELOG

#### v1.1: the initial version (v1.0 was the first mock-up I completely reworked)

#### v1.2:
- added a pocket for the stock X endstop. The connector socket has to be bent to face upwards. To do this, remove the plastic socket, then bent the pins upward and push the socket back on the pins. The socket housing will sit on the legs of the switch at the correct height, so don't force it all the way down to the PCB!
